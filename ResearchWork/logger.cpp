#include "logger.h"

std::string now(const char* format)
{
    std::time_t t = std::time(0);
    char cstr[128];
    std::strftime(cstr, sizeof(cstr), format, std::localtime(&t));
    return cstr;
}

void logEvent(std::string value)
{
    std::cout << "[" << now("%I:%M:%S") << "] [" << value << "]" << std::endl;
}

void logProcess(std::string value)
{
    std::cout << "[" << now("%I:%M:%S") << "] <" << value << ">" << std::endl;
}
