#include <QApplication>
#include <iostream>
#include <fstream>

#include <view/controlwidget.h>

#include <view/graph.h>

#include <view/simulation/mainroadwidget.h>
#include <view/twodimensionsgraphwidget.h>
#include <solution/data.h>
#include <view/solution-sample/solution_sample_widget.h>

#include "logger.h"

std::string fileName("/home/overtired/Desktop/somefile.txt");

void makeExpoerimentWithModel()
{
    for (int k = 0; k < 8; k++) {
        std::cout << "Experiment " << k + 1 << std::endl;

        for (int i = 0; i < 25; i += 1) {
            RoadModel* model = new RoadModel(4.0,
                                             2.0,
                                             UnitsUtils::fromMeters(1),
                                             UnitsUtils::fromMinuts(0.005),
                                             UnitsUtils::fromMinuts(0.01),
                                             UnitsUtils::fromMeters(10),
                                             3,
                                             true);
            model->onMainIntensivityChanged(1000);
            model->onMiddleIntensivityChanged(1000);
            model->onSlowIntensivityChanged(1000);
            model->onSpeedyIntensivityChanged(2000.0 / (100 - i)*i);
            model->onDelayChanged(0.0);

            model->run();
            std::cout << i << ";" << model->getCountOfWentCars() << std::endl;

            delete model;
        }
    }
}

void runModelSimulation()
{
    MainRoadWidget().show();
}

void runOneImitationAndSaveResults()
{
    RoadModel* model = new RoadModel(
        UnitsUtils::fromKilometers(10.0),
        UnitsUtils::fromHours(1.0),
        UnitsUtils::fromMeters(5),
        UnitsUtils::fromSeconds(1),
        UnitsUtils::fromMinuts(0.01),
        UnitsUtils::fromMeters(10),
        3,
        true);

    model->onMainIntensivityChanged(300); // no more than 300, please
    model->onMiddleIntensivityChanged(1);
    model->onSlowIntensivityChanged(1);
    model->onSpeedyIntensivityChanged(1);
    model->onDelayChanged(0.0);

    model->run();

    logEvent("Imitation finished");

    model->densityDistribution()->saveToFile(fileName);
}

void readDensityDistributionApproximateAndShow()
{
    Data densityDistribution = Data::loadFromFile(fileName);
    Data changedData = densityDistribution.approximateAndCut();
    TwoDimensionsGraphWidget widget(&densityDistribution, &changedData, false);
    TwoDimensionsGraphWidget widget2(&densityDistribution, &changedData, true);
    widget.show();
    widget2.show();
}

int main(int argc, char* argv[])
{
    // todo: make control widget
    QApplication a(argc, argv);


    logEvent("Program launched");

//    SolutionSampleWidget w;
//    w.show();
//    MainRoadWidget w;
//    w.show();
//    runOneImitationAndSaveResults();

    ControlWidget w;
    w.show();


//    Data densityDistribution = Data::loadFromFile(fileName);
//    Data changedData = densityDistribution.approximateAndCut();
//    TwoDimensionsGraphWidget widget(&densityDistribution, &changedData, false);
//    TwoDimensionsGraphWidget widget2(&densityDistribution, &changedData, true);
//    widget.show();
//    widget2.show();


    // remove these four words
    //    int returnValue = runOneImitationAndSaveResults();
    //    readDensityDistributionApproximateAndShow();
    //    int returnValue = runModelSimulation(argc, argv);
    return a.exec();
}
