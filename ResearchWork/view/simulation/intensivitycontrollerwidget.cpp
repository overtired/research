#include "view/simulation/intensivitycontrollerwidget.h"

IntensivityControllerWidget::IntensivityControllerWidget(QWidget* parent) : QWidget(parent)
{
    slowIntensivitySlider = new QSlider(Qt::Vertical);
    middleIntensivitySlider = new QSlider(Qt::Vertical);
    speedyIntensivitySlider = new QSlider(Qt::Vertical);
    mainIntensivitySlider = new QSlider(Qt::Vertical);

    initializeSlider(slowIntensivitySlider);
    initializeSlider(middleIntensivitySlider);
    initializeSlider(speedyIntensivitySlider);
    initializeSlider(mainIntensivitySlider);

    slowLabel = new QLabel("Slow");
    middleLabel = new QLabel("Middle");
    speedyLabel = new QLabel("Speedy");
    mainLabel = new QLabel("Main");

    layout = new QGridLayout(this);


    layout->addWidget(slowIntensivitySlider, 0, 0, Qt::AlignCenter);
    layout->addWidget(middleIntensivitySlider, 0, 1, Qt::AlignCenter);
    layout->addWidget(speedyIntensivitySlider, 0, 2, Qt::AlignCenter);
    layout->addWidget(mainIntensivitySlider, 0, 3, Qt::AlignCenter);

    layout->addWidget(slowLabel, 1, 0, Qt::AlignCenter);
    layout->addWidget(middleLabel, 1, 1, Qt::AlignCenter);
    layout->addWidget(speedyLabel, 1, 2, Qt::AlignCenter);
    layout->addWidget(mainLabel, 1, 3, Qt::AlignCenter);

    connect(slowIntensivitySlider, SIGNAL(valueChanged(int)), this,
            SLOT(onSlowIntensivityChanged(int)));
    connect(middleIntensivitySlider, SIGNAL(valueChanged(int)), this,
            SLOT(onMiddleIntensivityChanged(int)));
    connect(speedyIntensivitySlider, SIGNAL(valueChanged(int)), this,
            SLOT(onSpeedyIntensivityChanged(int)));
    connect(mainIntensivitySlider, SIGNAL(valueChanged(int)), this,
            SLOT(onMainIntensivityChanged(int)));
}

void IntensivityControllerWidget::onSlowIntensivityChanged(int intensivity)
{
    emit slowIntensivityChanged(intensivity);
}

void IntensivityControllerWidget::onMiddleIntensivityChanged(int intensivity)
{
    emit middleIntensivityChanged(intensivity);
}

void IntensivityControllerWidget::onSpeedyIntensivityChanged(int intensivity)
{
    emit speedyIntensivityChanged(intensivity);
}

void IntensivityControllerWidget::onMainIntensivityChanged(int intensivity)
{
    emit mainIntensivityChanged(intensivity);
}

void IntensivityControllerWidget::initializeSlider(QSlider* slider)
{
    slider->setMaximum(RoadModel::MAX_INTENSIVITY);
    slider->setMinimum(RoadModel::MIN_INTENSIVITY);
}
