#ifndef INTENSIVITYCONTROLLERWIDGET_H
#define INTENSIVITYCONTROLLERWIDGET_H

#include <QWidget>
#include <QSlider>
#include <QLabel>
#include <QGridLayout>

#include "solution/simulation/roadmodel.h"

class IntensivityControllerWidget : public QWidget {
    Q_OBJECT

  private:
    QGridLayout* layout;

    QSlider* slowIntensivitySlider;
    QSlider* middleIntensivitySlider;
    QSlider* speedyIntensivitySlider;
    QSlider* mainIntensivitySlider;

    QLabel* slowLabel;
    QLabel* middleLabel;
    QLabel* speedyLabel;
    QLabel* mainLabel;

  public:
    explicit IntensivityControllerWidget(QWidget* parent = nullptr);

  public slots:
    void onSlowIntensivityChanged(int intensivity);
    void onMiddleIntensivityChanged(int intensivity);
    void onSpeedyIntensivityChanged(int intensivity);
    void onMainIntensivityChanged(int intensivity);

  signals:
    void slowIntensivityChanged(int intensivity);
    void middleIntensivityChanged(int intensivity);
    void speedyIntensivityChanged(int intensivity);
    void mainIntensivityChanged(int intensivity);

  private:
    void initializeSlider(QSlider* slider);
};

#endif // INTENSIVITYCONTROLLERWIDGET_H
