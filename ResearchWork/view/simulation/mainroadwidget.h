#ifndef MAINROADWIDGET_H
#define MAINROADWIDGET_H

#include <QWidget>
#include <QObject>
#include <QBoxLayout>

#include "roadwidget.h"
#include "libraries/qcustomplot.h"
#include "solution/simulation/roadmodel.h"
#include "speedcontrollerwidget.h"
#include "intensivitycontrollerwidget.h"
#include "solution/unitsutils.h"

class MainRoadWidget : public QWidget {
    Q_OBJECT

  private:
    RoadModel* model;
    RoadWidget* roadWidget;
    SpeedControllerWidget* speedControllerWidget;
    IntensivityControllerWidget* intensivityControllerWidget;
    QVBoxLayout* verticalLayout;
    QHBoxLayout* horizontalLayout;
    QCustomPlot* densityPlot;

    //    const int countOfLines = 4;

  public:
    explicit MainRoadWidget(QWidget* parent = nullptr);

  signals:

  public slots:
};

#endif // MAINROADWIDGET_H
