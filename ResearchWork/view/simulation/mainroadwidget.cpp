#include "view/simulation/mainroadwidget.h"

MainRoadWidget::MainRoadWidget(QWidget* parent) : QWidget(parent)
{
    setWindowTitle("RoadSimulationModel");

    int countOfRoads = 3;
    double lengthLimit = UnitsUtils::fromKilometers(10);
    double timeLimit = UnitsUtils::fromHours(1);

    verticalLayout = new QVBoxLayout(this);
    horizontalLayout = new QHBoxLayout();
    roadWidget = new RoadWidget(countOfRoads, lengthLimit);
    speedControllerWidget = new SpeedControllerWidget(timeLimit);
    intensivityControllerWidget = new IntensivityControllerWidget();
    //densityPlot = new QCustomPlot();

    horizontalLayout->addWidget(speedControllerWidget);
    horizontalLayout->addWidget(intensivityControllerWidget);
    //horizontalLayout->addWidget(densityPlot);

    verticalLayout->addWidget(roadWidget);
    verticalLayout->addLayout(horizontalLayout);

    model = new RoadModel(lengthLimit,
                          timeLimit,
                          UnitsUtils::fromMeters(5),
                          UnitsUtils::fromSeconds(1),
                          UnitsUtils::fromMinuts(0.01),
                          UnitsUtils::fromMeters(10),
                          countOfRoads,
                          true);

    connect(model, SIGNAL(onNewState(QVector<Agent>*)), roadWidget, SLOT(newState(QVector<Agent>*)));
    connect(model, SIGNAL(onTimeChanged(double)), speedControllerWidget, SLOT(onTimeChanged(double)));
    connect(speedControllerWidget, SIGNAL(onDelayChanged(int)), model, SLOT(onDelayChanged(int)));
    connect(intensivityControllerWidget, SIGNAL(mainIntensivityChanged(int)), model,
            SLOT(onMainIntensivityChanged(int)));
    connect(intensivityControllerWidget, SIGNAL(slowIntensivityChanged(int)), model,
            SLOT(onSlowIntensivityChanged(int)));
    connect(intensivityControllerWidget, SIGNAL(middleIntensivityChanged(int)), model,
            SLOT(onMiddleIntensivityChanged(int)));
    connect(intensivityControllerWidget, SIGNAL(speedyIntensivityChanged(int)), model,
            SLOT(onSpeedyIntensivityChanged(int)));


    model->start();
}
