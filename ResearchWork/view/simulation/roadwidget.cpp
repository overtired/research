#include "view/simulation/roadwidget.h"

RoadWidget::RoadWidget(int countOfLines, double lengthLimit, QWidget* parent)
    : QWidget(parent),
      countOfLines{countOfLines},
      lengthLimit{lengthLimit},
      step{WINDOW_HEIGHT / countOfLines}

{
    setFixedSize(WINDOW_WIDTH, WINDOW_HEIGHT);

    blackDashPen.setWidth(ROAD_PEN_WIDTH);
    blackPen.setWidth(ROAD_PEN_WIDTH);
    agentPen.setWidth(AGENT_PEN_WIDTH);
}

void RoadWidget::draw(Agent agent)
{
    QPainter painter(this);
    int x = agent.X() / lengthLimit * width();
    int y = (countOfLines - agent.line() - 1) * step + step / 2;

    painter.setBrush(QBrush(agent.color()));
    painter.setPen(agentPen);
    //painter.drawEllipse(QPoint(x, y), AGENT_ELLIPSE_RADIUS, AGENT_ELLIPSE_RADIUS);
    double halfAgentLength = agent.length() / 2 / lengthLimit * width();

    painter.drawRect(x - halfAgentLength, y - AGENT_HALF_WIDTH, 2 * halfAgentLength,
                     2 * AGENT_HALF_WIDTH);
}

void RoadWidget::paintEvent(QPaintEvent* event)
{
    Q_UNUSED(event);

    drawLines();

    if (currentState != nullptr) {
        for (int i = 0; i < currentState->size(); i++) {
            draw(currentState->at(i));
        }
    }

    //    Agent slowAgent = Agent::getSlowAgent();
    //    slowAgent.setPosition(100);
    //    slowAgent.setLine(0);
    //    draw(slowAgent);

    //    Agent middleAgent = Agent::getMiddleAgent();
    //    middleAgent.setPosition(200);
    //    middleAgent.setLine(1);
    //    draw(middleAgent);

    //    Agent speedyAgent = Agent::getSpeedyAgent();
    //    speedyAgent.setPosition(300);
    //    speedyAgent.setLine(3);
    //    draw(speedyAgent);

}

void RoadWidget::newState(QVector<Agent>* agents)
{
    this->currentState = agents;
    this->update();
}

RoadWidget::~RoadWidget()
{

}

void RoadWidget::drawLines()
{
    QPainter painter(this);
    painter.setPen(blackPen);
    int xLeft = 0;
    int xRight = width();
    painter.drawLine(xLeft, 0 + ROAD_PEN_WIDTH, xRight, 0 + ROAD_PEN_WIDTH);
    painter.drawLine(xLeft, height() - ROAD_PEN_WIDTH, xRight, height() - ROAD_PEN_WIDTH);

    painter.setPen(blackDashPen);

    for (int i = 1; i < countOfLines; i++) {
        painter.drawLine(xLeft, i * step, xRight, i * step);
    }
}
