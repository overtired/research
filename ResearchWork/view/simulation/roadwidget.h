#ifndef ROADWIDGET_H
#define ROADWIDGET_H

#include <QWidget>
#include <QBoxLayout>
#include <QPainter>
#include <QPoint>
#include <QDebug>

#include "solution/simulation/modelstate.h"

class RoadWidget : public QWidget {
    Q_OBJECT

  private:
    static const int AGENT_HALF_WIDTH = 6;
    static const int AGENT_PEN_WIDTH = 2;
    static const int ROAD_PEN_WIDTH = 3;
    static const int WINDOW_HEIGHT = 200;
    static const int WINDOW_WIDTH = 1000;

  private:
    // Logic-objects
    int countOfLines;
    double lengthLimit;
    int step;

    QVector<Agent>* currentState = nullptr;

    // UI
    QVBoxLayout* mainLayout;
    QVBoxLayout* roadLayout;

    QPen blackPen = QPen(Qt::SolidLine);
    QPen blackDashPen = QPen(Qt::DotLine);
    QPen agentPen = QPen(Qt::SolidLine);

  public:
    RoadWidget(int countOfLines, double lengthLimit, QWidget* parent = 0);
    ~RoadWidget();

  private:

    void drawLines();
    void draw(Agent agent);

  protected:
    void paintEvent(QPaintEvent* event);

  public slots:
    void newState(QVector<Agent>* agents);

  signals:
    void timeChanged(double time);
};

#endif // ROADWIDGET_H
