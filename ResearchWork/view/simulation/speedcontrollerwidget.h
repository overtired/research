#ifndef SPEEDCONTROLLERWIDGET_H
#define SPEEDCONTROLLERWIDGET_H

#include <QWidget>
#include <QSlider>
#include <QBoxLayout>
#include <QLabel>

class SpeedControllerWidget : public QWidget {
    Q_OBJECT

  private:
    const int minDelay = 5;
    const int maxDelay = 20;

    QSlider* speedSlider;
    QVBoxLayout* layout;
    QLabel* speedLabel;
    QLabel* timeLabel;
    QLabel* modelTime;

    double maxTime;

  public:
    explicit SpeedControllerWidget(double maxTime, QWidget* parent = nullptr);

  signals:
    void onDelayChanged(int delay);

  public slots:
    void onSpeedSliderChangedValue(int value);
    void onTimeChanged(double time);
};

#endif // SPEEDCONTROLLERWIDGET_H
