#include "view/simulation/speedcontrollerwidget.h"

SpeedControllerWidget::SpeedControllerWidget(double maxTime, QWidget* parent) : QWidget(parent),
    maxTime{maxTime}
{
    layout = new QVBoxLayout(this);

    speedSlider = new QSlider(Qt::Horizontal);
    speedSlider->setMinimum(minDelay);
    speedSlider->setMaximum(maxDelay);
    speedLabel = new QLabel("Simulation speed");
    timeLabel = new QLabel("Simulation time");
    modelTime = new QLabel("0.0/" + QString::number(maxTime));

    layout->addWidget(speedLabel, 0, Qt::AlignCenter);
    layout->addWidget(speedSlider, 1, Qt::AlignCenter);
    layout->addWidget(timeLabel, 0, Qt::AlignCenter);
    layout->addWidget(modelTime, 0, Qt::AlignCenter);

    connect(speedSlider, SIGNAL(sliderMoved(int)), this, SLOT(onSpeedSliderChangedValue(int)));
}

void SpeedControllerWidget::onSpeedSliderChangedValue(int value)
{
    emit onDelayChanged(maxDelay - value + 1);
}

void SpeedControllerWidget::onTimeChanged(double time)
{
    modelTime->setText(QString::number(time / maxTime) + "%");
}
