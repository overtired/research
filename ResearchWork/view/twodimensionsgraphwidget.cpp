#include "twodimensionsgraphwidget.h"

TwoDimensionsGraphWidget::TwoDimensionsGraphWidget(Data* data, Data* data2, bool shouldReverse,
                                                   QWidget* parent)
{
    this->data = data;
    this->shouldReverse = shouldReverse;

    layout = new QVBoxLayout(this);
    plot = new QCustomPlot();

    float yAxisRange = 5;

    if (shouldReverse) {
        //        plot->xAxis->setRange(0.0f, data->getTimeLimit());
        plot->yAxis->setRange(-yAxisRange, yAxisRange);
        plot->xAxis->setRange(0.0f, 0.05f);
    } else {
        //        plot->xAxis->setRange(0.0f, data->getLengthLimit());
        plot->xAxis->setRange(0.0f, 0.5f);
        plot->yAxis->setRange(-yAxisRange, yAxisRange);
    }


    if (data2 != nullptr) {
        this->data2 = data2;
        plot2 = new QCustomPlot();

        if (shouldReverse) {
            //            plot2->xAxis->setRange(0.0f, data2->getTimeLimit());
            plot2->yAxis->setRange(-yAxisRange, yAxisRange);
            plot2->xAxis->setRange(0.0f, 0.05f);
        } else {
            //            plot2->xAxis->setRange(0.0f, data2->getLengthLimit());
            plot2->yAxis->setRange(-yAxisRange, yAxisRange);
            plot2->xAxis->setRange(0.0f, 0.5f);
        }
    }

    slider = new QSlider(Qt::Horizontal);

    if (shouldReverse) {
        slider->setMaximum(data2->countOfStepsByX() - 1);
    } else {
        slider->setMaximum(data2->countOfStepsByT() - 1);
    }

    slider->setMinimum(0);

    layout->addWidget(plot);

    if (plot2 != nullptr) {
        layout->addWidget(plot2);
    }

    layout->addWidget(slider);

    connect(slider, SIGNAL(valueChanged(int)), this, SLOT(onSliderMoved(int)));
    onSliderMoved(0);

    setFixedSize(800, 800);
}

void TwoDimensionsGraphWidget::onSliderMoved(int position)
{
    QPen pen;
    pen.setWidth(1);

    plot->clearGraphs();
    plot->addGraph();

    if (shouldReverse) {
        QVector<double> values;

        for (int i = 0; i < data->countOfStepsByT(); i++) { //todo something there????
            values.push_back(data->getTimeLayer(i)[position + 1]);
        }

        QVector<double> xValues = data->getValuesOfArgumentByT();


        plot->graph(0)->addData(xValues, values);
    } else {
        QVector<double> xValues = data->getValuesOfArgumentByX();
        xValues.pop_back();
        xValues.pop_front();

        QVector<double> values = data->getTimeLayer(position + 1);
        values.pop_back();
        values.pop_front();

        plot->graph(0)->addData(xValues, values);
    }

    plot->graph(0)->setPen(pen);
    plot->graph(0)->setScatterStyle(QCPScatterStyle::ssDisc);

    if (plot2 != nullptr) {
        plot2->clearGraphs();
        plot2->addGraph();
        plot2->graph(0)->setPen(pen);

        if (shouldReverse) {
            QVector<double> values;

            for (int i = 0; i < data2->countOfStepsByT(); i++) {
                values.push_back(data2->getTimeLayer(i)[position]);
            }

            plot2->graph(0)->addData(data2->getValuesOfArgumentByT(), values);
        } else {
            plot2->graph(0)->addData(data2->getValuesOfArgumentByX(), data2->getTimeLayer(position));
        }

        plot2->graph(0)->setScatterStyle(QCPScatterStyle::ssDisc);
        plot2->replot();
    }

    plot->replot();
}
