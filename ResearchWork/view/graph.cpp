#include "graph.h"

Graph::Graph(QWidget* parent) : QWidget(parent)
{
    layout = new QVBoxLayout(this);
    plot = new QCustomPlot();
    plot->addGraph();

    QVector<double> vector;
    vector.push_back(0);
    vector.push_back(1);
    vector.push_back(2);
    vector.push_back(3);
    vector.push_back(4);

    plot->graph(0)->addData(vector, vector);
    layout->addWidget(plot);
}
