#ifndef GRAPH_H
#define GRAPH_H

#include <QBoxLayout>
#include <QVector>

#include "libraries/qcustomplot.h"

class Graph : public QWidget {
    Q_OBJECT
  public:
    explicit Graph(QWidget* parent = 0);

  signals:

  public slots:

  private:
    QCustomPlot* plot = nullptr;
    QBoxLayout* layout = nullptr;
};

#endif // GRAPH_H
