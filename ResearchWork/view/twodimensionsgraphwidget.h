#ifndef TWODIMENSIONSGRAPHWIDGET_H
#define TWODIMENSIONSGRAPHWIDGET_H

#include <QSlider>
#include <QVBoxLayout>

#include "libraries/qcustomplot.h"
#include "solution/data.h"

class TwoDimensionsGraphWidget : public QWidget {
    Q_OBJECT
  public:
    explicit TwoDimensionsGraphWidget(Data* data, Data* data2 = nullptr, bool shouldReverse = false,
                                      QWidget* parent = 0);

  private:

    // Data
    Data* data = nullptr;
    Data* data2 = nullptr;

    // Widgets
    QSlider* slider = nullptr;
    QCustomPlot* plot = nullptr;
    QCustomPlot* plot2 = nullptr;
    QVBoxLayout* layout = nullptr;

    bool shouldReverse = false;

  signals:

  public slots:
    void onSliderMoved(int position);
};

#endif // TWODIMENSIONSGRAPHWIDGET_H
