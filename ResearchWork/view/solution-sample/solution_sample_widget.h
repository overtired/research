#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#define QCUSTOMPLOT_USE_LIBRARY

#include <QWidget>
#include <QBoxLayout>
#include <QSlider>
#include <QDebug>
#include <QProgressBar>
#include <QLabel>

#include "libraries/qcustomplot.h"

#include "solution/equation/equationsolver.h"
#include "solution/equation/defaultfunctions.h"

class SolutionSampleWidget : public QWidget {
    Q_OBJECT

  private:
    // Solution parameters
    const double h = 0.01;
    const double tau = 0.001;
    const double xMin = 0.0;
    const double xMax = 5.0;
    const double tMin = 0.0;
    const double tMax = 5.0;

    // Ui parameters
    const int WINDOW_WIDTH = 1000;
    const int WINDOW_HEIGHT = 400;

    // Ui strings
    QString sTitle = "ResearchWork Gruzintsev A.S. MM-14-2";

    // Widgets
    QVBoxLayout* llVertical = new QVBoxLayout();
    QHBoxLayout* llHorizontal = new QHBoxLayout();
    QCustomPlot* cpSolution = new QCustomPlot();
    QCustomPlot* cpExpectedFunction = new QCustomPlot();
    QSlider* slider = new QSlider(Qt::Horizontal);
    QProgressBar* progress = new QProgressBar();

    // Data containers
    QVector<QVector<double>> result;
    QVector<QVector<double>> expectedResult;
    QVector<double> xValues;
    QVector<QVector<double>> error;

    EquationSolver* solver = new EquationSolver();

  public:
    SolutionSampleWidget(QWidget* parent = 0);

  private slots:
    void onSolved();
    void onSliderMoved(int value);
};

#endif // MAINWIDGET_H
