#include "solution_sample_widget.h"

SolutionSampleWidget::SolutionSampleWidget(QWidget* parent) : QWidget (parent)
{
    llVertical->addWidget(progress);
    progress->setMinimum(0);
    progress->setMaximum(100);

    setFixedSize(WINDOW_WIDTH, 40);

    setLayout(llVertical);

    setWindowTitle(sTitle);

    QVector<double> minT;
    QVector<double> maxT;
    QVector<double> minX;

    for (double value_t = tMin; value_t < tMax; value_t += tau) {
        minT.append(defStartA(value_t));
        maxT.append(defStartB(value_t));
    }

    for (double value_x = xMin; value_x < xMax; value_x += h) {
        minX.append(defStart0(value_x));
    }

//        solver->initialize(tau, h, tMin, tMax, xMin, xMax,
//                           defStartA, defStartB, defStart0, defQ, defF);
    solver->initialize(tMin, tMax, xMin, xMax, minT, maxT, minX, defQ, defF);

    solver->start();

    cpSolution->xAxis->setRange(-1, 6);
    cpSolution->yAxis->setRange(-2, 10);

    cpExpectedFunction->xAxis->setRange(-1, 6);
    cpExpectedFunction->yAxis->setRange(-0.01, 0.05);

    connect(solver, SIGNAL(solved()), this, SLOT(onSolved()));
    connect(solver, SIGNAL(onPercentChanged(int)), progress, SLOT(setValue(int)));
    connect(slider, SIGNAL(valueChanged(int)), this, SLOT(onSliderMoved(int)));
}

void SolutionSampleWidget::onSolved()
{
    llVertical->removeWidget(progress);
    llHorizontal->addWidget(cpSolution);
    llHorizontal->addWidget(cpExpectedFunction);
    llVertical->addLayout(llHorizontal);
    llVertical->addWidget(slider);

    setFixedSize(WINDOW_WIDTH, WINDOW_HEIGHT);

    slider->setRange(0, solver->getCountByT() - 1);

    for (int i = 0; i < solver->getCountByX(); i++) {
        xValues.append(i * solver->getH());
    }

    result = solver->getResult();

    expectedResult = QVector<QVector<double>>();
    error = QVector<QVector<double>>();

    for (double t = tMin; t < tMax; t += tau) {
        expectedResult.append(QVector<double>());
        error.append(QVector<double>());

        for (double x = xMin; x < xMax; x += h) {
            double defaultValue = defRo(x, t);
            expectedResult[expectedResult.size() - 1].append(defaultValue);
            error[error.size() - 1].append(fabs(defaultValue - result[t / tau][x / h]));
        }
    }

    onSliderMoved(0);
}

void SolutionSampleWidget::onSliderMoved(int value)
{
    QPen pen;
    pen.setWidth(2);

    pen.setColor(QColor(0, 0, 0));

    cpSolution->clearGraphs();

    cpSolution->addGraph();
    cpSolution->graph(0)->addData(xValues, result[value]);
    cpSolution->graph(0)->setPen(pen);
    //    cpSolution->graph(0)->setScatterStyle(QCPScatterStyle::ssDisc);

    pen.setColor(QColor(0, 255, 0));
    cpSolution->addGraph();
    cpSolution->graph(1)->addData(xValues, expectedResult[value]);
    cpSolution->graph(1)->setPen(pen);
    //    cpSolution->graph(1)->setScatterStyle(QCPScatterStyle::ssDisc);

    cpSolution->replot();

    pen.setColor(QColor(0, 0, 255));
    cpExpectedFunction->clearGraphs();
    cpExpectedFunction->addGraph();
    cpExpectedFunction->graph(0)->addData(xValues, error[value]);
    cpExpectedFunction->graph(0)->setPen(pen);
    //    cpExpectedFunction->graph(0)->setScatterStyle(QCPScatterStyle::ssDisc);
    cpExpectedFunction->replot();
}
