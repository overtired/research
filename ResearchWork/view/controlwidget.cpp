#include "controlwidget.h"

ControlWidget::ControlWidget(QWidget *parent) : QWidget(parent)
{
    layout = new QVBoxLayout(this);

    simulateAndShowButton = new QPushButton("Show simulation");
    simulateAndSaveButton = new QPushButton("Run simulation and save");
    simulateAndShowAndSaveButton = new QPushButton("Show simulation and save");
    testSolutionButton = new QPushButton("Show solution example");

    layout->addWidget(simulateAndShowButton);
    layout->addWidget(simulateAndSaveButton);
    layout->addWidget(simulateAndShowAndSaveButton);
    layout->addWidget(testSolutionButton);

    setFixedSize(300, 200);

    connect(simulateAndShowButton,SIGNAL(released()), this, SLOT(simulateAndShow()));
    connect(simulateAndSaveButton,SIGNAL(released()), this, SLOT(simulateAndSave()));
    connect(simulateAndShowAndSaveButton,SIGNAL(released()), this, SLOT(simulateAndShowAndSave()));
    connect(testSolutionButton,SIGNAL(released()), this, SLOT(testSolution()));
}

void ControlWidget::simulateAndShow(){
    if(mainRoadWidget != nullptr){
        delete mainRoadWidget;
    }
    mainRoadWidget = new MainRoadWidget();
    mainRoadWidget->show();
}

void ControlWidget::simulateAndSave(){

}

void ControlWidget::simulateAndShowAndSave(){

}

void ControlWidget::testSolution(){
    if(sampleWidget != nullptr){
        delete sampleWidget;
    }
    sampleWidget = new SolutionSampleWidget();
    sampleWidget->show();
}
