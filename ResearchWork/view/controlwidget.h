#ifndef CONTROLWIDGET_H
#define CONTROLWIDGET_H

#include <QWidget>

#include <QPushButton>
#include <QVBoxLayout>

#include "view/simulation/mainroadwidget.h"
#include "view/solution-sample/solution_sample_widget.h"

class ControlWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ControlWidget(QWidget *parent = 0);

private:
    QPushButton* simulateAndShowButton = nullptr;
    QPushButton* simulateAndSaveButton = nullptr;
    QPushButton* simulateAndShowAndSaveButton = nullptr;
    QPushButton* testSolutionButton = nullptr;

    QVBoxLayout* layout = nullptr;

    MainRoadWidget* mainRoadWidget = nullptr;
    SolutionSampleWidget* sampleWidget = nullptr;


private slots:
    void simulateAndShow();
    void simulateAndSave();
    void simulateAndShowAndSave();
    void testSolution();
};

#endif // CONTROLWIDGET_H
