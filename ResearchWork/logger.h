#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <iostream>
#include <ctime>

std::string now(const char* format = "%c");

void logEvent(std::string value);

void logProcess(std::string value);

#endif // LOGGER_H
