#-------------------------------------------------
#
# Project created by QtCreator 2017-09-22T17:34:26
#
#-------------------------------------------------

QT       += core gui printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ResearchWork
TEMPLATE = app

# Uncomment line below to link boost libraries
# LIBS += -lboost_system -lboost_thread -L/usr/lib/ -lboost_serialization -lboost_wserialization


# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
# DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++14

SOURCES += main.cpp \
    libraries/qcustomplot.cpp \
    solution/equation/defaultfunctions.cpp \
    solution/equation/equationsolver.cpp \
    solution/simulation/agent.cpp \
    solution/data.cpp \
    solution/simulation/modelstate.cpp \
    solution/simulation/roadmodel.cpp \
    solution/unitsutils.cpp \
    view/simulation/intensivitycontrollerwidget.cpp \
    view/simulation/mainroadwidget.cpp \
    view/simulation/roadwidget.cpp \
    view/simulation/speedcontrollerwidget.cpp \
    view/twodimensionsgraphwidget.cpp \
    view/graph.cpp \
    logger.cpp \
    view/solution-sample/solution_sample_widget.cpp \
    solution/equation/functionq.cpp \
    view/controlwidget.cpp

HEADERS  += mainwidget.h \
    libraries/qcustomplot.h \
    solution/equation/defaultfunctions.h \
    solution/equation/equationsolver.h \
    solution/simulation/agent.h \
    solution/data.h \
    solution/simulation/modelstate.h \
    solution/simulation/roadmodel.h \
    solution/unitsutils.h \
    view/simulation/intensivitycontrollerwidget.h \
    view/simulation/mainroadwidget.h \
    view/simulation/roadwidget.h \
    view/simulation/speedcontrollerwidget.h \
    view/twodimensionsgraphwidget.h \
    view/graph.h \
    logger.h \
    view/solution-sample/solution_sample_widget.h \
    solution/equation/functionq.h \
    view/controlwidget.h


