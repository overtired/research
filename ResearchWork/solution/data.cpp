#include "solution/data.h"

Data::Data() {}

Data::Data(int countOfRoads, int countOfStepByT, int countOfStepsByX, double stepT, double stepX):
    countOfRoads{countOfRoads},
    timeLimit{countOfStepByT * stepT},
    lengthLimit{countOfStepsByX * stepX},
    stepT{stepT},
    stepX{stepX}
{
    data = QVector<QVector<double>>(countOfStepByT, QVector<double>(countOfStepsByX));
    initializeValuesOfArgument();
}

Data::Data(int countOfRoads, double timeLimit,
           double lengthLimit,
           double stepT,
           double stepX,
           QVector<QVector<double> > data):
    countOfRoads{countOfRoads},
    timeLimit{timeLimit},
    lengthLimit{lengthLimit},
    stepT{stepT},
    stepX{stepX},
    data{data}
{
    initializeValuesOfArgument();
}

Data::Data(const Data& data)
{
    this->countOfRoads = data.countOfRoads;
    this->timeLimit = data.timeLimit;
    this->lengthLimit = data.lengthLimit;
    this->stepT = data.stepT;
    this->stepX = data.stepX;
    this->data = data.data;
    initializeValuesOfArgument();
}

void Data::setTimeLayer(int index, QVector<double> layer)
{
    data[index] = layer;
}

QVector<double> Data::getTimeLayer(int index)
{
    return data[index];
}

QVector<double> Data::getValuesOfArgumentByX()
{
    return valuesOfArgumentByX;
}

QVector<double> Data::getValuesOfArgumentByT()
{
    return valuesOfArgumentByT;
}

int Data::countOfStepsByT()
{
    return data.size();
}

int Data::countOfStepsByX()
{
    return data.at(0).size();
}

double Data::getLengthLimit()
{
    return lengthLimit;
}

double Data::getTimeLimit()
{
    return timeLimit;
}

double Data::getStepX()
{
    return stepX;
}

void Data::saveToFile(const std::string& fileName)
{
    logProcess("Writing data to file started");

    std::ofstream file(fileName);
    writeToStream(file);

    file.close();

    logEvent("Loading data to file finished");
}

Data Data::loadFromFile(const std::string& fileName)
{
    logProcess("Reading data from file started");

    std::ifstream file(fileName);
    Data data;
    data.readFromStream(file);
    data.initializeValuesOfArgument();
    file.close();

    logEvent("Reading data from file finished");

    return data;
}

std::ofstream& Data::writeToStream(std::ofstream& stream)
{
    stream << this->countOfRoads << std::endl
           << this->timeLimit << std::endl
           << this->lengthLimit << std::endl
           << this->stepT << std::endl
           << this->stepX << std::endl;

    for (int i = 0; i < data.size(); i++) {
        for (int j = 0; j < data[i].size(); j++) {
            stream << data[i][j] << std::endl;
        }
    }

    return stream;
}

std::ifstream& Data::readFromStream(std::ifstream& stream)
{
    stream >> this->countOfRoads
           >> this->timeLimit
           >> this->lengthLimit
           >> this->stepT
           >> this->stepX;

    int limitByLength(lengthLimit / stepX);
    int limitByTime(timeLimit / stepT);

    for (int i = 0; i < limitByTime; i++) {
        data.append(QVector<double>());

        for (int j = 0; j < limitByLength; j++) {
            double value;
            stream >> value;
            data[i].append(value);
        }
    }

    return stream;
}

Data Data::approximateAndCut()
{
    logProcess("Data approximation started");
    logEvent("Dimensions of the matrix are " +
             std::to_string(countOfStepsByT()) +
             " by time and " +
             std::to_string(countOfStepsByX()) +
             " by length");
    Data newData(countOfRoads, countOfStepsByT() - 2, countOfStepsByX() - 2, stepT, stepX);

    //todo should be less steps

    newData.data = approximateAndCut(data, countOfRoads);

    logEvent("Data approximation finished");

    return newData;
}

void Data::initializeValuesOfArgument()
{
    valuesOfArgumentByX.clear();
    valuesOfArgumentByT.clear();

    for (int i = 0; i < countOfStepsByX(); i++) {
        valuesOfArgumentByX.push_back(i * stepX);
    }

    for (int i = 0; i < countOfStepsByT(); i++) {
        valuesOfArgumentByT.push_back(i * stepT);
    }
}

QVector<QVector<double> > Data::approximateAndCut(QVector<QVector<double>>& vector, int degree)
{
    QVector<QVector<double>> temp = vector;

    //    for (int i = 0; i < degree - 1; i++) {
    for (int i = 0; i < 1; i++) {

        QVector<QVector<double>> approximatedL2R = temp;
        QVector<QVector<double>> approximatedR2L = temp;
        QVector<QVector<double>> approximatedS2E = temp;
        QVector<QVector<double>> approximatedE2S = temp;

        for (int i_layer = 1; i_layer < countOfStepsByT() - 1; i_layer++) {
            for (int i_value = 0; i_value < countOfStepsByX(); i_value++) {
                approximatedS2E[i_layer][i_value]
                    = (approximatedS2E[i_layer + 1][i_value] +
                       approximatedS2E[i_layer][i_value] +
                       approximatedS2E[i_layer - 1][i_value]) / 3.0;
                approximatedE2S[countOfStepsByT() - 1 - i_layer][countOfStepsByX() - 1 - i_value] =
                    (approximatedE2S[countOfStepsByT() - 1 - i_layer - 1][countOfStepsByX() - 1 - i_value] +
                     //                     approximatedE2S[countOfStepsByT() - 1 - i_layer][countOfStepsByX() - 1 - i_value] +
                     approximatedE2S[countOfStepsByT() - 1 - i_layer + 1][countOfStepsByX() - 1 - i_value]) / 2.0;

            }
        }

        for (int i_layer = 0; i_layer < countOfStepsByT(); i_layer++) {
            for (int i_value = 1; i_value < countOfStepsByX() - 1; i_value++) {
                approximatedL2R[i_layer][i_value] = (approximatedL2R[i_layer][i_value + 1] +
                                                     approximatedL2R[i_layer][i_value] +
                                                     approximatedL2R[i_layer][i_value - 1]) /
                                                    3.0;
                approximatedR2L[countOfStepsByT() - 1 - i_layer][countOfStepsByX() - 1 - i_value]
                    = (
                          approximatedR2L[countOfStepsByT() - 1 - i_layer][countOfStepsByX() - 1 - i_value
                                                                           - 1] +
                          //                          approximatedR2L[countOfStepsByT() - 1 - i_layer][countOfStepsByX() - 1 - i_value] +
                          approximatedR2L[countOfStepsByT() - 1 - i_layer][countOfStepsByX() - 1 - i_value + 1]
                      ) / 2.0;
            }
        }

        for (int i_layer = 0; i_layer < countOfStepsByT(); i_layer++) {
            for (int i_value = 0; i_value < countOfStepsByX(); i_value++) {
                temp[i_layer][i_value] = (approximatedL2R[i_layer][i_value] * 0.25 +
                                          approximatedR2L[i_layer][i_value] * 0.25 +
                                          approximatedS2E[i_layer][i_value] * 0.25 +
                                          approximatedE2S[i_layer][i_value] * 0.25
                                         );
            }
        }
    }

    QVector<QVector<double>> result = QVector<QVector<double>>(countOfStepsByT() - 2,
                                                               QVector<double>(countOfStepsByX() - 2));


    for (int i_layer = 1; i_layer < countOfStepsByT() - 1; i_layer++) {
        for (int i_value = 1; i_value < countOfStepsByX() - 1; i_value++) {
            result[i_layer - 1][i_value - 1] = temp[i_layer][i_value];
        }
    }

    return result;
}
