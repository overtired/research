#include "function.h"

Function::Function(int index, int type, double a, double b):
    index_{index},
    type_{type},
    a_{a},
    b_{b}
{
    if(index_<0)
    {
        throw std::domain_error("Wrong index of basic function!");
    }
    if(type!=TYPE::POLINOM && type!=TYPE::SIN)
    {
        throw std::domain_error("Wrong type of function!");
    }
}

Function::Function(int index, int type):
    index_{index},
    type_{type}
{
    if(index_<0)
    {
        throw std::domain_error("Wrong index of basic function!");
    }
    if(type!=TYPE::POLINOM && type!=TYPE::SIN)
    {
        throw std::domain_error("Wrong type of function!");
    }
    if(type==TYPE::SIN)
    {
        throw std::domain_error("Add range for sin!");
    }
}

Function::Function():
    index_{0},
    type_{0}
{
}

double Function::operator ()(double x)
{
    switch (type_)
    {
    case TYPE::POLINOM:
        return pow(x,index_);
        break;

    case TYPE::SIN:
        if(index_==0)
        {
            return 1;
        }
        else
        {
            return sin(index_*x*M_PI/(b_-a_));
        }
        break;
    }
}
