#ifndef APPROXIMATOR_H
#define APPROXIMATOR_H

#include <stdexcept>
#include <cmath>
#include <QThread>
#include <unistd.h>

#include <QDebug>

#include "matrix.h"
#include "function.h"
#include "counter.h"

typedef double (*f_x)(double);

class Approximator
{    

public:
    enum TYPE:int { SIN = 0, POLINOM = 1 };
    Approximator(TYPE t, int countOfFunctions, f_x F, double a, double b);
    ~Approximator();    
    double operator()(double x) const;

private:
    Matrix *matrix;
    double a_;
    double b_;
    double h_ = 0.01;
    int count_;
    f_x F;
    Function *phi_;        

    double scalar(Function f1, f_x f2);
    double scalar(Function f1, Function f2);
};

#endif // APPROXIMATOR_H
