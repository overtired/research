#include "matrix.h"

Matrix::Matrix(int dimension):
    dimension_{dimension}
{
    if(dimension_<1)
    {
        throw std::domain_error("Wrong dimension in matrix!");
    }
    x_ = new double[dimension_];
    b_ = new double[dimension_];
    array_ = new double*[dimension_];
    for(int i=0;i<dimension_;i++)
    {
        array_[i] = new double[dimension_];
    }

}

Matrix::~Matrix()
{
    delete[] x_;
    delete[] b_;
    for(int i=0;i<dimension_;i++)
    {
        delete[] array_[i];
    }
    delete[] array_;
}

double Matrix::getX(int row)
{
    return x_[row];
}

void Matrix::setA(int row, int col, double value)
{
    array_[row][col] = value;
}

void Matrix::setB(int row, double value)
{
    b_[row] = value;
}

void Matrix::solveByGauss()
{
    for(int i=0;i<dimension_;i++)
    {
        double max = array_[i][i];
        int index = i;
        for(int j=i+1;j<dimension_;j++)
        {
            if(fabs(array_[j][i])>fabs(max))
            {
                max = array_[j][i];
                index = j;
            }
        }
        swapLines(i,index);
        if(fabs(max)<0.001)
        {
            continue;
        }

        for(int j=i+1;j<dimension_;j++)
        {
            if(fabs(array_[i][j])>0.001)
            {
                linePlusAnotherWithCoef(i,j,-array_[j][i]/array_[i][i]);
            }
        }
    }

//    Обратный ход
    x_[dimension_-1] = b_[dimension_-1]/array_[dimension_-1][dimension_-1];
    for(int i=dimension_-2;i>=0;i--)
    {
        double temp =0;
        for(int j=dimension_-1;j>i;j--)
        {
            temp +=array_[i][j]*x_[j];
        }
        x_[i] = (b_[i] - temp)/array_[i][i];
    }
}

void Matrix::linePlusAnotherWithCoef(int rowWhat, int rowTo, double koeff)
{
    for(int i=0;i<dimension_;i++)
    {
        array_[rowTo][i] += koeff*array_[rowWhat][i];
    }
    b_[rowTo] += b_[rowWhat]*koeff;
}

void Matrix::swapLines(int line1, int line2)
{
    double b=b_[line1];
    b_[line1] = b_[line2];
    b_[line2] = b;

    for(int i=0;i<dimension_;i++)
    {
        double a = array_[line1][i];
        array_[line1][i] = array_[line2][i];
        array_[line2][i] = a;
    }

}




