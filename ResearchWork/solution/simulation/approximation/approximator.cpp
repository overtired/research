#include "approximator.h"

Approximator::Approximator(Approximator::TYPE t, int countOfFunctions, f_x F, double a, double b):
    a_{a},
    b_{b},
    count_{countOfFunctions}
{
    if (count_ < 1) {
        throw std::domain_error("Too few arguments for approximations!");
    }

    if (b <= a) {
        throw std::domain_error("Wrong interval!");
    }

    //    Создаю базисные функции
    phi_ = new Function[count_];

    for (int i = 0; i < count_; i++) {
        if (t == Approximator::TYPE::SIN) {
            phi_[i] = Function(i, t, a_, b_);
        } else {
            phi_[i] = Function(i, t);
        }
    }

    //    Заполняю матрицу для Гаусса
    //    Тут старый рабочий способ, пробую новым

    //    matrix = new Matrix(count_);
    //    for(int i=0;i<count_;i++)
    //    {
    //        for(int j=0;j<count_;j++)
    //        {
    //            matrix->setA(i,j,scalar(phi_[i],phi_[j]));
    //        }
    //        matrix->setB(i,scalar(phi_[i],F));
    //    }

    //    matrix->solveByGauss();

    //    Заставляю их считать мои функции
    matrix = new Matrix(count_);
    Counter counters[count_];

    for (int i = 0; i < count_; i++) {
        counters[i].setValues(phi_, phi_[i], a_, b_, h_, count_);
        counters[i].run();
        matrix->setB(i, scalar(phi_[i], F));
    }

    //    Тут в этом же потоке считаю B
    for (int i = 0; i < count_; i++) {
        matrix->setB(i, scalar(phi_[i], F));
    }

    //    Теперь жду результатов от все остальных потоков и заполняю матрицу
    int j = 0;

    while (j < count_) {
        if (counters[j].isFinished()) {
            QVector<double> result = counters[j].getResult();

            for (int i = 0; i < count_; i++) {
                matrix->setA(j, i, result.at(i));
            }

            j++;
        } else {
            sleep(1);
        }
    }

    matrix->solveByGauss();
}

Approximator::~Approximator()
{
    delete[] phi_;
    delete matrix;
}

double Approximator::operator()(double x) const
{
    double temp = 0;

    for (int i = 0; i < count_; i++) {
        temp += phi_[i](x) * matrix->getX(i);
    }

    return temp;
}

double Approximator::scalar(Function f1, f_x f2)
{
    double temp = 0;

    for (double i = a_; i < b_; i += h_) {
        //        a x^2 + b x + c = f(x)
        Matrix m = Matrix(3);

        m.setA(0, 0, i * i);
        m.setA(0, 1, i);
        m.setA(0, 2, 1);
        m.setB(0, f1(i)*f2(i));

        m.setA(1, 0, (i + h_ / 2) * (i + h_ / 2));
        m.setA(1, 1, i + h_ / 2);
        m.setA(1, 2, 1);
        m.setB(1, f1(i + h_ / 2)*f2(i + h_ / 2));

        m.setA(2, 0, (i + h_) * (i + h_));
        m.setA(2, 1, i + h_);
        m.setA(2, 2, 1);
        m.setB(2, f1(i + h_)*f2(i + h_));

        m.solveByGauss();

        temp += m.getX(0) / 3 * (pow(i + h_, 3) - pow(i, 3)) + m.getX(1) / 2 * (pow(i + h_, 2) - pow(i,
                                                                                                     2)) + m.getX(2) * h_;
    }

    qDebug() << "result of scal = " << temp;
    return temp;
}

double Approximator::scalar(Function f1, Function f2)
{
    double temp = 0;

    for (double i = a_; i < b_; i += h_) {
        //        a * x^2 + b * x + c = f(x)
        Matrix m = Matrix(3);

        m.setA(0, 0, i * i);
        m.setA(0, 1, i);
        m.setA(0, 2, 1);
        m.setB(0, f1(i)*f2(i));

        m.setA(1, 0, (i + h_ / 2) * (i + h_ / 2));
        m.setA(1, 1, i + h_ / 2);
        m.setA(1, 2, 1);
        m.setB(1, f1(i + h_ / 2)*f2(i + h_ / 2));

        m.setA(2, 0, (i + h_) * (i + h_));
        m.setA(2, 1, i + h_);
        m.setA(2, 2, 1);
        m.setB(2, f1(i + h_)*f2(i + h_));

        m.solveByGauss();

        qDebug() << m.getX(0) << m.getX(1) << m.getX(2);

        temp += m.getX(0) / 3 * (pow(i + h_, 3) - pow(i, 3)) + m.getX(1) / 2 * (pow(i + h_, 2) - pow(i,
                                                                                                     2)) + m.getX(2) * h_;
    }

    return temp;
}


