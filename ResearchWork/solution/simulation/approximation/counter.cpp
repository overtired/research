#include "counter.h"

Counter::Counter()
{

}

void Counter::run()
{
    result_.clear();
    for(int i=0;i<count_;i++)
    {
        result_.push_back(scalar(functions_[i],function_));
    }
    finished_ = true;
}

double Counter::scalar(Function f1, Function f2)
{
    double temp = 0;
    for(double i=a_;i<b_;i+=h_)
    {
//        a x^2 + b x + c = f(x)
        Matrix m = Matrix(3);

        m.setA(0,0,i*i);
        m.setA(0,1,i);
        m.setA(0,2,1);
        m.setB(0,f1(i)*f2(i));

        m.setA(1,0,(i+h_/2)*(i+h_/2));
        m.setA(1,1,i+h_/2);
        m.setA(1,2,1);
        m.setB(1,f1(i+h_/2)*f2(i+h_/2));

        m.setA(2,0,(i+h_)*(i+h_));
        m.setA(2,1,i+h_);
        m.setA(2,2,1);
        m.setB(2,f1(i+h_)*f2(i+h_));

        m.solveByGauss();
        temp+=m.getX(0)/3*(pow(i+h_,3)-pow(i,3))+m.getX(1)/2*(pow(i+h_,2)-pow(i,2))+m.getX(2)*h_;
    }
    return temp;
}

void Counter::setValues(Function *functions, Function function, double a, double b, double h, int count)
{
    functions_ = functions;
    function_ = function;
    a_ = a;
    b_ = b;
    h_= h;
    count_ = count;
}

QVector<double> Counter::getResult()
{
    return result_;
}

bool Counter::isFinished() const
{
    return finished_;
}
