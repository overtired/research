#ifndef FUNCTION_H
#define FUNCTION_H

#include <stdexcept>
#include <cmath>

class Function
{
public:
    enum TYPE:int { SIN = 0, POLINOM = 1 };
    Function(int index, int type);
    Function(int index, int type,double a, double b);
    Function();
    double operator ()(double x);

private:
    int index_;
    int type_;
    double a_;
    double b_;
};
#endif // FUNCTION_H
