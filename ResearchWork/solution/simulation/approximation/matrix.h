#ifndef MATRIX_H
#define MATRIX_H

#include <cmath>
#include <stdexcept>

class Matrix
{
public:
    Matrix(int dimension);
    ~Matrix();
    double getX(int row);
    void setA(int row, int col, double value);
    void setB(int row, double value);
    void solveByGauss();
    void linePlusAnotherWithCoef(int rowWhat, int rowTo, double koeff);
    void swapLines(int line1, int line2);

private:
    double **array_;
    double *x_;
    double *b_;
    int dimension_;
};

#endif // MATRIX_H
