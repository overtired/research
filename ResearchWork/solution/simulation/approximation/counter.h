#ifndef COUNTER_H
#define COUNTER_H

#include <QVector>
#include <QThread>

#include "matrix.h"
#include "function.h"

class Counter:QThread
{
    Q_OBJECT

public:
    Counter();
    void run();
    void setValues(Function *functions, Function function, double a, double b, double h, int count);
    QVector<double> getResult();
    bool isFinished() const;

private:
    bool finished_ = false;
    int count_;
    double a_;
    double b_;
    double h_;
    Function *functions_;
    Function function_;
    double scalar(Function f1, Function f2);
    QVector<double> result_;
};

#endif // COUNTER_H
