#include "solution/simulation/agent.h"

Agent::Agent()
{

}

Agent::Agent(const Agent& agent):
    previousPosition{agent.previousPosition},
    currentPosition{agent.currentPosition},
    agentColor{agent.agentColor},
    carLength{agent.carLength},
    previousVelocity{agent.previousVelocity},
    currentVelocity{agent.currentVelocity},
    currentLine{agent.currentLine},
    carWidth{agent.carWidth},
    preferedVelocity{agent.preferedVelocity},
    maxAcceleration{agent.maxAcceleration},
    maxVelocity{agent.maxVelocity},
    comfortableDeceleration{agent.comfortableDeceleration},
    agressivity{agent.agressivity},
    countOfLastCrosses{agent.countOfLastCrosses}
{
}

Agent::Agent(double width,
             double length,
             double prefVelocity,
             double previousVelocity,
             double maxVelocity,
             double comfDeceleration,
             double maxAcceleration,
             QColor agentColor,
             double agressivity)
{
    this->carWidth = width;
    this->carLength = length;
    this->preferedVelocity = prefVelocity;
    this->previousVelocity = prefVelocity;
    this->maxVelocity = maxVelocity;
    this->comfortableDeceleration = comfDeceleration;
    this->maxAcceleration = maxAcceleration;
    this->agentColor = agentColor;
    this->agressivity = agressivity;

    currentPosition = 0.0;
    previousPosition = 0.0;
    currentVelocity = prefVelocity;
}

Agent::~Agent()
{
}

void Agent::setLine(int line)
{
    if (countOfLastCrosses == 0) {
        currentLine = line;
        countOfLastCrosses += 2;
    } else {
        countOfLastCrosses--;
    }

}

void Agent::setX(double position)
{
    previousPosition = currentPosition;
    currentPosition = position;
}

void Agent::setV(double speed)
{
    previousVelocity = currentVelocity;
    currentVelocity = speed;
}

int Agent::line()
{
    return currentLine;
}

double Agent::X() const
{
    return currentPosition;
}

double Agent::V() const
{
    return currentVelocity;
}

double Agent::length() const
{
    return carLength;
}

double Agent::width() const
{
    return carWidth;
}

QColor Agent::color()
{
    return agentColor;
}

void Agent::setColor(QColor color)
{
    this->agentColor = color;
}

Agent Agent::newSlowAgent()
{
    return Agent(UnitsUtils::fromMeters(2.5),
                 UnitsUtils::fromMeters(4.5),
                 UnitsUtils::fromKmPerHour(60),
                 UnitsUtils::fromKmPerHour(60),
                 UnitsUtils::fromKilometers(80),
                 UnitsUtils::fromKmPerHourForSecond(1),
                 UnitsUtils::fromKmPerHourForSecond(3),
                 Qt::green,
                 0.1);
}

Agent Agent::newMiddleAgent()
{
    return Agent(UnitsUtils::fromMeters(2.2),
                 UnitsUtils::fromMeters(5),
                 UnitsUtils::fromKmPerHour(80),
                 UnitsUtils::fromKmPerHour(80),
                 UnitsUtils::fromKilometers(100),
                 UnitsUtils::fromKmPerHourForSecond(3),
                 UnitsUtils::fromKmPerHourForSecond(7),
                 Qt::yellow,
                 0.4);
}

Agent Agent::newSpeedyAgent()
{
    return Agent(UnitsUtils::fromMeters(2.4),
                 UnitsUtils::fromMeters(4.2),
                 UnitsUtils::fromKmPerHour(100),
                 UnitsUtils::fromKmPerHour(100),
                 UnitsUtils::fromKilometers(120),
                 UnitsUtils::fromKmPerHourForSecond(6),
                 UnitsUtils::fromKmPerHourForSecond(12),
                 Qt::red,
                 0.6);
}

bool Agent::operator <(const Agent t1) const
{
    return X() < t1.X();
}

double Agent::maxA() const
{
    return maxAcceleration;
}

double Agent::maxV() const
{
    return maxVelocity;
}

double Agent::comfD() const
{
    return comfortableDeceleration;
}

double Agent::getAgressivity() const
{
    return agressivity;
}

double Agent::getPreviousVelocity() const
{
    return previousVelocity;
}

double Agent::getPreviousPosition() const
{
    return previousPosition;
}

double Agent::getPreferedVelocity() const
{
    return preferedVelocity;
}
