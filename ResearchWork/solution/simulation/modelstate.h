#ifndef MODELSTATE_H
#define MODELSTATE_H

#include "solution/simulation/agent.h"

class ModelState {
  public:
    ModelState(QVector<Agent>* agents);

    QVector<Agent>* getAgents();
  private:
    QVector<Agent>* agents;

};

#endif // MODELSTATE_H
