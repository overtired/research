#ifndef ROADMODEL_H
#define ROADMODEL_H

#include <QThread>
#include <QVector>
#include <QDebug>
#include <QDateTime>

#include <cmath>
#include <string>

#include "solution/simulation/agent.h"
#include "solution/simulation/modelstate.h"
#include "solution/data.h"

#include "libraries/qcustomplot.h"

class RoadModel: public QThread {
    Q_OBJECT

  public:
    static const int MAX_INTENSIVITY = 1000;
    static const int MIN_INTENSIVITY = 1;

  private:
    static constexpr double minDistanceBetweenCars = 0.002;

    int delay = 500;
    double currentTime = 0.0;

    Data* data;

    double tau;
    double h;
    double lengthLimit;
    double timeLimit;
    double timeGap;
    double jamGap;
    int countOfRoads;
    bool allowToCrossLine;

    int countOfWentCars;

    int slowIntensivity = MIN_INTENSIVITY;
    int middleIntensivity = MIN_INTENSIVITY;
    int speedyIntensivity = MIN_INTENSIVITY;
    int mainIntensivity = MIN_INTENSIVITY;

    QVector<Agent> agents;

  private:

    bool shouldAddAgent();
    Agent getNewAgent();
    void addAgentInFreeSpace(Agent agent);
    bool hasSpeedyCarOnLineBehind(int line, int index);

    double countAcceleration(Agent& a, Agent& b);

    bool canCrossLine(Agent& ca, Agent& na, int position, int line);

    void moveCurrentAgents();
    void addNewAgentIfNeed();
    void saveLayer(int index);

  public:
    RoadModel(double lengthLimit,
              double timeLimit,
              double h,
              double tau,
              double timeGap,
              double jamGap,
              int countOfRoads,
              bool allowToCrossLine);
    ~RoadModel();

    double getLengthLimit();
    double getTimeLimit();

    bool needToChangeLine(int index);
    bool canAhead(int index);
    bool canOutrun(int index);

    int getCountOfWentCars() const;

    Data* densityDistribution();

    void run();

  public slots:
    void onDelayChanged(int delay);
    void onSlowIntensivityChanged(int intensivity);
    void onMiddleIntensivityChanged(int intensivity);
    void onSpeedyIntensivityChanged(int intensivity);
    void onMainIntensivityChanged(int intensivity);

  signals:
    void onNewState(QVector<Agent>* state);
    void onTimeChanged(double time);
};

#endif // ROADMODEL_H
