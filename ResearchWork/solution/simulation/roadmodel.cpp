#include "solution/simulation/roadmodel.h"

RoadModel::RoadModel(double lengthLimit,
                     double timeLimit,
                     double h,
                     double tau,
                     double timeGap,
                     double jamGap,
                     int countOfRoads, bool allowToCrossLine):
    lengthLimit{lengthLimit},
    timeLimit{timeLimit},
    h{h},
    tau{tau},
    timeGap{timeGap},
    jamGap{jamGap},
    countOfRoads{countOfRoads},
    allowToCrossLine{allowToCrossLine}
{
    qsrand(QDateTime::currentMSecsSinceEpoch());

    data = new Data(countOfRoads, timeLimit / tau, lengthLimit / h, tau, h);
}

RoadModel::~RoadModel()
{
}

double RoadModel::getLengthLimit()
{
    return lengthLimit;
}

double RoadModel::getTimeLimit()
{
    return timeLimit;
}

bool RoadModel::shouldAddAgent()
{
    int value = qrand() % MAX_INTENSIVITY;
    return value < mainIntensivity;
}

Agent RoadModel::getNewAgent()
{
    int value = qrand() % (slowIntensivity + middleIntensivity +
                           speedyIntensivity);

    if (value < slowIntensivity) {
        return Agent::newSlowAgent();
    } else if (value < slowIntensivity + middleIntensivity) {
        return Agent::newMiddleAgent();
    } else {
        return Agent::newSpeedyAgent();
    }
}

void RoadModel::addAgentInFreeSpace(Agent agent)
{
    for (int i = 0; i < agents.size(); i++) {
        if (agents[i].line() == agent.line()) {
            if (agents[i].X() - agents[i].length() - jamGap - agent.length() > 0.0) {
                agents.append(agent);
            }

            return;
        }
    }

    agents.append(agent);
}

bool RoadModel::hasSpeedyCarOnLineBehind(int line, int index)
{
    for (int i = index - 1; i > agents.size(); i--) {
        if (agents[i].line() == line) {
            return agents[i].V() * tau + agents[i].X() >
                   agents[index].X() - agents[index].length() - minDistanceBetweenCars;
        }
    }

    return true;
}

double RoadModel::countAcceleration(Agent& a, Agent& b)
{
    double deltaVelocityCA = b.getPreviousVelocity() - a.V();
    double distanceToNA = b.getPreviousPosition() - a.X() - a.length();
    double desiredDistance = jamGap + fmax(a.V() * tau - (a.V() * deltaVelocityCA) / (2 * sqrt(
                                                                                          a.maxA() * a.comfD())), 0);

    double acceleartionCA = a.maxA() * (1.0 - pow(a.V() / a.maxV(),
                                                  4) - pow(desiredDistance / distanceToNA, 2));
    return acceleartionCA;
}

bool RoadModel::canCrossLine(Agent& ca, Agent& na, int position, int line)
{
    if (ca.line() != line && line >= 0 && line < countOfRoads) {
        Agent* pa = nullptr;
        Agent* paLine = nullptr;
        Agent* naLine = nullptr;

        for (int k = position - 1; k >= 0; k--) {
            if (agents[k].line() == ca.line()) {
                pa = &agents[k];
                break;
            }
        }

        for (int k = position - 1; k >= 0; k--) {
            if (agents[k].line() == line) {
                paLine = &agents[k];
                break;
            }
        }

        for (int k = position + 1; k < agents.size(); k++) {
            if (agents[k].line() == line) {
                naLine = &agents[k];
                break;
            }
        }

        double diff = 0.0;

        if (naLine == nullptr && paLine == nullptr) {
            return true;
        }

        if (naLine != nullptr) {
            diff += countAcceleration(ca, *naLine);
            diff -= countAcceleration(ca, na);
        }

        if (naLine != nullptr && paLine != nullptr) {
            diff += ca.getAgressivity() * countAcceleration(*paLine, ca);
            diff -= ca.getAgressivity() * countAcceleration(*paLine, *naLine);
        }

        if (pa != nullptr) {
            diff += ca.getAgressivity() * countAcceleration(*pa, na);
            diff -= ca.getAgressivity() * countAcceleration(*pa, ca);
        }

        return diff > 10000;
    }

    return false;
}

void RoadModel::moveCurrentAgents()
{
    // Counting for averrage speed
    double averageSpeed = 0.0;

    for (int i = 0; i < agents.size(); i++) {
        averageSpeed += agents[i].getPreviousVelocity();
    }

    averageSpeed /= agents.size();
    // Moving current agents

    for (int i = agents.size() - 1; i >= 0; i--) {

        Agent& ca = agents[i];

        double currentPosition = ca.X();

        // Removing unvisible agents
        if (currentPosition > lengthLimit) {
            agents.remove(i);
            countOfWentCars++;
            continue;
        }

        currentPosition = currentPosition + averageSpeed * tau;// pref veloc

        for (int j = i + 1; j < agents.size(); j++) {
            Agent& na = agents[j];

            if (na.line() == ca.line()) {
                double acceleartionCA = countAcceleration(ca, na);

                currentPosition = ca.X() + ca.V() * tau +
                                  acceleartionCA * tau * tau / 2;

                double velocity = ca.V() + acceleartionCA * tau;

                if (velocity < 0) {
                    ca.setV(0.0);
                } else {
                    ca.setV(velocity);
                }

                double maxPosition = na.getPreviousPosition() - jamGap - ca.length();

                if (currentPosition >= maxPosition) {
                    currentPosition = maxPosition;
                }

                if (allowToCrossLine && (ca.V() - ca.getPreferedVelocity() > 0)) {
                    if (canCrossLine(ca, na, i, ca.line() - 1)) {
                        ca.setLine(ca.line() - 1);
                    } else if (canCrossLine(ca, na, i, ca.line() + 1)) {
                        ca.setLine(ca.line() + 1);
                    }
                }

                break;
            }
        }

        agents[i].setX(currentPosition);
    }
}

void RoadModel::addNewAgentIfNeed()
{
    if (shouldAddAgent()) {
        Agent agent = getNewAgent();
        agent.setLine(qrand() % countOfRoads);
        addAgentInFreeSpace(agent);
    }
}

void RoadModel::saveLayer(int index)
{
    QVector<double> newDensity(lengthLimit / h, 0.0f);

    for (Agent agent : agents) {
        int index_local = (agent.X() / h) + 1;

        while (index_local >= 0 && index_local < newDensity.size() &&
                index_local * h < agent.X() + agent.length()) {
            newDensity[index_local++]++;
        }
    }

    data->setTimeLayer(index, newDensity);
}

void RoadModel::onDelayChanged(int delay)
{
    this->delay = delay * 50;
}

void RoadModel::onSlowIntensivityChanged(int intensivity)
{
    slowIntensivity = intensivity;
}

void RoadModel::onMiddleIntensivityChanged(int intensivity)
{
    middleIntensivity = intensivity;
}

void RoadModel::onSpeedyIntensivityChanged(int intensivity)
{
    speedyIntensivity = intensivity;
}

void RoadModel::onMainIntensivityChanged(int intensivity)
{
    logEvent("New main intensivity = " + std::to_string(intensivity));
    mainIntensivity = intensivity;
}

int RoadModel::getCountOfWentCars() const
{
    return countOfWentCars;
}

Data* RoadModel::densityDistribution()
{
    return data;
}

void RoadModel::run()
{
    countOfWentCars = 0;
    int counter = 0;

    while (currentTime < timeLimit) {

        int temp = (int)((currentTime / timeLimit) * 100);

        if (temp != counter) {
            counter = temp;
            logProcess("Imitation " + std::to_string(counter) + "% ...");
        }

        qStableSort(agents);
        addNewAgentIfNeed();
        qStableSort(agents);

        moveCurrentAgents();

        saveLayer(currentTime / tau);


        emit onNewState(&agents);
        emit onTimeChanged(currentTime);

        currentTime += tau;
        msleep(delay);
    }
}
