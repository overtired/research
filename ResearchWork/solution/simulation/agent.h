#ifndef AGENT_H
#define AGENT_H

#include <QColor>

#include "solution/unitsutils.h"

class Agent {
  public:
    static Agent newSlowAgent();
    static Agent newMiddleAgent();
    static Agent newSpeedyAgent();
    static const int SPEED_INCREMENT = 20;

    Agent();
    Agent(const Agent& agent);
    Agent(double width,
          double length,
          double prefVelocity,
          double previousVelocity,
          double maxVelocity,
          double comfDeceleration,
          double maxAcceleration,
          QColor agentColor,
          double getAgressivity);
    ~Agent();

    void setLine(int line);
    void setX(double X);
    void setV(double speed);

    int line();
    double X() const;
    double V() const;
    double length() const;
    double width() const;

    QColor color();

    void setColor(QColor agentColor);

    bool operator <(const Agent t1) const;

    double maxA() const;
    double maxV() const;
    double comfD() const;
    double getAgressivity() const;
    double getPreviousVelocity() const;
    double getPreviousPosition() const;
    double getPreferedVelocity() const;

  private:
    double carWidth;
    double carLength;

    double preferedVelocity;

    int currentLine;
    double currentPosition;
    double currentVelocity;

    double previousVelocity;
    double previousPosition;

    double comfortableDeceleration;
    double maxAcceleration;
    double maxVelocity;
    double agressivity;

    int countOfLastCrosses = 0;

    QColor agentColor;
};

#endif // AGENT_H
