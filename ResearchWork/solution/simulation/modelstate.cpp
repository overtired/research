#include "solution/simulation/modelstate.h"

ModelState::ModelState(QVector<Agent>* agents)
{
    this->agents = agents;
}

QVector<Agent>* ModelState::getAgents()
{
    return agents;
}
