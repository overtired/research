#include "defaultfunctions.h"

double defRo(double t, double x)
{
    return -0.1 * t * t - 0.1 * x * x + 5;
}

double defQ(double rho)
{
    return 1.0 / (pow(rho + 1, 3));
}

double defStartA(double t)
{
    return defRo(t, 0.0);
}

double defStartB(double t)
{
    return defRo(t, 5.0);
}

double defStart0(double x)
{
    return defRo(0.0, x);
}

double defF(double t, double x)
{
    double rho = defRo(t, x);
    return -0.2 * t - 0.2 * x * ((1.0 - 2.0 * rho) / (pow(rho + 1, 4)));
}
