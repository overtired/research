#include "solution/equation/equationsolver.h"

EquationSolver::EquationSolver()
{
}

EquationSolver::~EquationSolver()
{

}

void EquationSolver::initialize(double tau,
                                double h,
                                double t1,
                                double t2,
                                double x1,
                                double x2,
                                double (*startA)(double),
                                double (*startB)(double),
                                double (*start0)(double),
                                double (*Q)(double),
                                double (*f)(double t, double x))
{
    if (x2 < x1) {
        QString text = "Wrong limits, x2 < x1, x1 = "
                       + QString::number(x1) + " x2 = " + QString::number(x2);
        throw std::runtime_error(text.toStdString());
    }

    if (t2 < t1) {
        QString text = "Wrong limits, t2 < t1, t1 = "
                       + QString::number(t1) + " t2 = " + QString::number(t2);
        throw std::runtime_error(text.toStdString());
    }

    this->tau = tau;
    this->h = h;
    this->t1 = t1;
    this->t2 = t2;
    this->x1 = x1;
    this->x2 = x2;
    this->startA = startA;
    this->startB = startB;
    this->start0 = start0;
    this->Q = Q;
    this->f = f;

    // Initialization
    countByT = (t2 - t1) / tau + 1; // Adding a unit to process the latest edge of range
    countByX = (x2 - x1) / h + 1; // The same

    // Zeroing the solution
    initializeZeroes(countByT, countByX);

    // Setting start conditions by t
    for (int i = 0; i < countByX; i++) {
        result[0][i] = start0(x1 + i * h);
    }

    // Setting start and end conditions by x
    for (int i = 0; i < countByT; i++) {
        result[i][0] = startA(t1 + i * tau);
        result[i][countByX - 1] = startB(t1 + i * tau);
    }
}

void EquationSolver::initialize(
    double minT,
    double maxT,
    double minX,
    double maxX,
    const QVector<double>& startT,
    const QVector<double>& endT,
    const QVector<double>& startX,
    double (*Q)(double),
    double (*f)(double, double)
)
{
    if (startT.size() != endT.size()) {
        QString text = "Wrong limits for solution conditions minT: "
                       + QString::number(startT.size())
                       + " and maxT: "
                       + QString::number(endT.size());
        throw std::runtime_error(text.toStdString());
    }

    if (fabs(startT.first() - startX.first()) > eta || fabs(endT.first() - startX.last()) > eta) {
        throw std::runtime_error("Conditions are not same in vertexes");
    }

    this->tau = (maxT - minT) / double(startT.size());
    this->h = (maxX - minX) / double(startX.size());
    this->t1 = minT;
    this->t2 = maxT;
    this->x1 = minX;
    this->x2 = maxX;
    this->Q = Q;
    this->f = f;

    initializeZeroes(startT.size(), startX.size());

    result.first() = startX;

    for (int i = 0; i < startT.size(); i++) {
        result[i].first() = startT[i];
        result[i].last() = endT[i];
    }

    countByT = startT.size();
    countByX = startX.size();
}

void EquationSolver::run()
{
    // Solution
    for (int t = 1; t < countByT; t++) {
        // Prediction
        for (int x = 1; x < countByX - 1; x++) {
            result[t][x] = result[t - 1][x] - 1.0 * tau * (result[t - 1][x + 1] - result[t - 1][x - 1]) /
                           (2.0 * h) *
                           (Q(result[t - 1][x]) + 1.0 * result[t - 1][x] / (2.0 * eta) * (Q(result[t - 1][x] + eta) - Q(
                                                                                              result[t - 1][x] - eta)));
        }

        // Start solving scheme by Thomas algorithm
        QVector<double> A = QVector<double>();
        QVector<double> B = QVector<double>();

        double expressinonWithQ = (Q(result[t][1]) + result[t][1] / (2 * eta) * (Q(
                                                                                     result[t][1] + eta) - Q(
                                                                                     result[t][1] - eta))) / (2.0 * h);

        A.append(-1.0 * expressinonWithQ * tau);
        double d = result[t - 1][1] / tau + f(t1 + tau * t, x1 + h);
        B.append((d + expressinonWithQ * result[t][0]) * tau);

        for (int x = 2; x < countByX - 2; x++) {
            expressinonWithQ = (Q(result[t][x]) + result[t][x] / (2 * eta) * (Q(
                                                                                  result[t][x] + eta) - Q(
                                                                                  result[t][x] - eta))) / (2.0 * h);
            double e = (-1.0) * A.last() * expressinonWithQ + 1.0 / tau;
            A.append(-1.0 * expressinonWithQ / e);
            d = result[t - 1][x] / tau + f(t1 + tau * t, x1 + x * h);
            B.append((d + expressinonWithQ * B.last()) / e);
        }

        expressinonWithQ = (Q(result[t][countByX - 2]) + result[t][countByX - 2] / (2 * eta) * (Q(
                                                                                                    result[t][countByX - 2] + eta) - Q(
                                                                                                    result[t][countByX - 2] - eta))) / (2.0 * h);

        d = result[t - 1][countByX - 2] / tau - result[t][countByX - 1] * expressinonWithQ + f(
                t1 + tau * t, x2 - h);

        result[t][countByX - 2] = (d + expressinonWithQ * B.last()) / ((1.0) / tau - expressinonWithQ *
                                                                       A.last());

        for (int x = countByX - 3; x >= 1; x--) {
            result[t][x] = A[x - 1] * result[t][x + 1] + B[x - 1];
        }

        emit this->onPercentChanged(t * 1.0 / (countByT - 1) * 100);
    }

    emit solved();
}

void EquationSolver::initializeZeroes(int byT, int byX)
{
    result.clear();

    for (int i = 0; i < byT; i++) {
        QVector<double> vec = QVector<double>();

        for (int j = 0; j < byX; j++) {
            vec.append(0.0);
        }

        result.append(vec);
    }
}

int EquationSolver::getCountByX()
{
    return countByX;
}

double EquationSolver::getH()
{
    return h;
}

double EquationSolver::getTau()
{
    return tau;
}

QVector<QVector<double> > EquationSolver::getResult()
{
    return result;
}

int EquationSolver::getCountByT()
{
    return countByT;
}
