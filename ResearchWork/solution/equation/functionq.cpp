#include "functionq.h"

FunctionQ::FunctionQ(int n)
{
    for (int i = 0; i < n; i++) {
        koefficents.append(QVector<double>(3, 0.0));
    }
}

FunctionQ::FunctionQ(QVector<QVector<double> > koefficents)
{
    if (koefficents.size() == 0) {
        throw std::runtime_error("Koefficents vector is empty");
    }

    for (const auto& setOfKoefs : koefficents) {
        if (setOfKoefs.size() != 3) {
            throw std::runtime_error("One of koefficent sets contains no three elements");
        }
    }

    this->koefficents = koefficents;
}

double FunctionQ::operator()(double value)
{
    double result = 0.0;

    for (const auto& setOfKoefs : koefficents) {
        result += setOfKoefs[0] * (atan(-setOfKoefs[1] * value + setOfKoefs[2]));
    }
}
