#ifndef DEFAULTFUNCTIONS_H
#define DEFAULTFUNCTIONS_H

#include <cmath>

extern double defRo(double t, double x);

extern double defQ(double t);

extern double defStartA(double t);

extern double defStartB(double t);

extern double defStart0(double x);

extern double defF(double t, double x);

#endif // DEFAULTFUNCTIONS_H
