#ifndef EQUATIONSOLVER_H
#define EQUATIONSOLVER_H

#include <QThread>
#include <QDebug>
#include <QVector>
#include <exception>
#include <functional>

#include "solution/equation/defaultfunctions.h"

class EquationSolver : public QThread {

    Q_OBJECT

  private:
    const double eta = 0.001;
    double tau;
    double h;
    double t1, t2;
    double x1, x2;

    int countByT;
    int countByX;

    double (*start0)(double);
    double (*startA)(double);
    double (*startB)(double);
    double (*f)(double t, double x);
    double (*Q)(double);

    QVector<QVector<double>> result;



  public:
    EquationSolver();
    ~EquationSolver();

    void initialize(double tau, double h, double t1, double t2, double x1, double x2,
                    double (*startA)(double),
                    double (*startB)(double),
                    double (*start0)(double),
                    double (*Q)(double),
                    double (*f)(double t, double x));

    void initialize(double minT,
                    double maxT,
                    double minX,
                    double maxX,
                    const QVector<double>& startT,
                    const QVector<double>& endT,
                    const QVector<double>& startX,
                    double (*Q)(double),
                    double (*f)(double, double));

    int getCountByT();
    int getCountByX();

    double getH();
    double getTau();

    QVector<QVector<double>> getResult();

    void run();

  private:
    void initializeZeroes(int byT, int byX);

  signals:
    void onPercentChanged(int percent);
    void solved();
};

#endif // EQUATIONSOLVER_H
