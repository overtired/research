#ifndef FUNCTIONQ_H
#define FUNCTIONQ_H

#include <cmath>
#include <exception>
#include <QVector>

class FunctionQ {
  public:
    FunctionQ(int n);
    FunctionQ(QVector<QVector<double>> koefficents);

    double operator()(double value);
  private:
    QVector<QVector<double>> koefficents;
};

#endif // FUNCTIONQ_H
