#ifndef UNITSUTILS_H
#define UNITSUTILS_H


class UnitsUtils {
  private:
    UnitsUtils();

  public:
    static double fromKilometers(double distance);
    static double fromMeters(double distance);
    static double fromHours(double time);
    static double fromMinuts(double time);
    static double fromSeconds(double time);
    static double fromKmPerHour(double velocity);
    static double fromMetersPerSecond(double velocity);
    static double fromKmPerHourForSecond(double acceleration);
};

#endif // UNITSUTILS_H
