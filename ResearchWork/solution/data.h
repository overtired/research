#ifndef DATA_H
#define DATA_H

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

#include <QDataStream>
#include <QVector>
#include <QFile>
#include <QDebug>

#include "logger.h"

/**
 * @brief The Data class
 * The container for the simulation data for the whole simulation time
 * and the solution data
 */
class Data {

  public:
    Data();
    Data(int countOfRoads,
         int countOfStepByT,
         int countOfStepsByX,
         double stepT,
         double stepX);

    Data(int countOfRoads,
         double timeLimit,
         double lengthLimit,
         double stepT,
         double stepX,
         QVector<QVector<double>> data);

    Data(const Data& data);

    void setTimeLayer(int index, QVector<double> layer);
    QVector<double> getTimeLayer(int index);

    QVector<double> getValuesOfArgumentByX();
    QVector<double> getValuesOfArgumentByT();

    int countOfStepsByT();
    int countOfStepsByX();

    double getLengthLimit();
    double getTimeLimit();

    double getStepX();

    void saveToFile(const std::string& fileName);

    static Data loadFromFile(const std::__cxx11::string& fileName);

    std::ofstream& writeToStream(std::ofstream& datastream);
    std::ifstream& readFromStream(std::ifstream& datastream);

    Data approximateAndCut();

  private:
    QVector<QVector<double>> data;
    QVector<double> valuesOfArgumentByX;
    QVector<double> valuesOfArgumentByT;

    double timeLimit;
    double lengthLimit;
    double stepT;
    double stepX;

    int countOfRoads;

  private:
    void initializeValuesOfArgument();
    QVector<QVector<double>> approximateAndCut(QVector<QVector<double>>& vector, int degree);
};

#endif // DATA_H
