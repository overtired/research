#include "solution/unitsutils.h"

UnitsUtils::UnitsUtils()
{
}

double UnitsUtils::fromKilometers(double distance)
{
    return distance;
}

double UnitsUtils::fromMeters(double distance)
{
    return distance / 1000;
}

double UnitsUtils::fromHours(double time)
{
    return time;
}

double UnitsUtils::fromMinuts(double time)
{
    return time / 60;
}

double UnitsUtils::fromSeconds(double time)
{
    return time / 3600;
}

double UnitsUtils::fromKmPerHour(double velocity)
{
    return velocity;
}

double UnitsUtils::fromMetersPerSecond(double velocity)
{
    return velocity * 3.6;
}

double UnitsUtils::fromKmPerHourForSecond(double acceleration)
{
    return acceleration * 3600;
}
